# Parser to covert to HTML
import re


class SlackwareParser:
    def markdown(self, string):
        # Convert Slackware syntax into html
        string = re.sub("^!([^!].*)$", "<h1>\\1</h1>", string, flags=re.MULTILINE)
        string = re.sub("^!!([^!].*)$", "<h2>\\1</h2>", string, flags=re.MULTILINE)
        string = re.sub("^!!!([^!].*)$", "<h3>\\1</h3>", string, flags=re.MULTILINE)
        string = re.sub(r"\*(.+)$", "<p>\\1</p>", string, flags=re.MULTILINE)
        string = re.sub(
            r"\[(.*?)\]\s*\((.*?)\)", "<a href=\\2>\\1</a>", string, flags=re.MULTILINE
        )
        return string

    def strip(self, string):
        string = re.sub(r"<(script).*?</\1>(?s)", "", string, flags=re.MULTILINE)
        return string
