from slackware_parser import SlackwareParser
import pytest  # type: ignore
import wiki
import os
import pathlib


@pytest.fixture
def client():
    wiki.app.config["TESTING"] = True

    with wiki.app.test_client() as client:
        yield client


def test_import():
    assert wiki is not None


def test_homepage(client):
    resp = client.get("/")
    assert resp.status_code == 200


# Make sure that there is some expected file
# contents are present in a response to /view/
def test_view(monkeypatch, client):
    monkeypatch.setattr(wiki, "PAGES_CONST", "test")
    resp = client.get("/view/")

    assert resp.status_code == 200


# Make sure that some expected file
# contents are present in a response to /view/PageName
def test_file(monkeypatch, client):
    monkeypatch.setattr(wiki, "PAGES_CONST", "test/pages")
    resp = client.get("/view/League of Legends")
    assert resp.status_code == 200
    assert b"Comparisons persisted between the game" in resp.data


def test_view_page_not_exitst(client):
    resp = client.get("/view/DontExistTest")

    assert resp.status_code == 200
    assert os.path.isfile("pages/DontExistTest.txt")
    os.remove("pages/DontExistTest.txt")


# Test Slackware's formatting
@pytest.mark.parametrize(
    ["slackware", "html"],
    [
        ("!Heading level 1", "<h1>Heading level 1</h1>"),
        ("!!Heading level 2", "<h2>Heading level 2</h2>"),
        ("!!!Heading level 3", "<h3>Heading level 3</h3>"),
        ("!!!!Wrong Heading Format", "!!!!Wrong Heading Format"),
        ('[example]("example.com")', '<a href="example.com">example</a>'),
        ("*Paragraph 1", "<p>Paragraph 1</p>"),
    ],
)
def test_formatting(slackware, html):
    parser = SlackwareParser()

    with open("test/pages/SourceCode.txt", "r") as f:
        content = f.read()

    content = parser.markdown(content)

    with open("test/pages/SourceCode.txt", "r") as f:
        htmlContent = f.read()

    assert content == htmlContent


def test_history(client):

    # Access non-existant file
    client.get("history/Test")
    assert not os.path.isfile("history/TestHistory")

    # Accessing history that is made
    client.get("history/SourceCode")


def test_edit(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test/pages"
    monkeypatch.setattr(wiki, "PAGES_CONST", test_dir)
    monkeypatch.chdir("test")

    resp = client.post(
        "/edit/TestPage",
        data=dict(
            content="The edit was a success",
            description="Testing edit",
            name="admin",
            email="test@email.com",
            submit=True,
        ),
        follow_redirects=True,
    )
    assert resp.status_code == 200
    with open("pages/TestPage.txt", "r") as f:
        content = f.read()
    assert content == "The edit was a success"


def test_edit_get(client, monkeypatch):
    monkeypatch.setattr(wiki, "PAGES_CONST", "test/pages")

    resp = client.get("/edit/TestPage")
    assert resp.status_code == 200
