from flask import Flask, flash, redirect, url_for
from flask import render_template
from slackware_parser import SlackwareParser
from forms import EditForm
from datetime import datetime
from pathlib import Path
import flask
import subprocess
import os
import csv

app = Flask(__name__)
app.config["SECRET_KEY"] = "you-will-never-guess"
PAGES_CONST = "pages"


@app.route("/")
def main():
    return render_template(
        "home.html",
        page_name="Home",
    )


# Requests to localhost:8080/view/ return text from a file named FrontPage
@app.route("/view/")
def viewfunc():
    return render_template(
        "home.html",
        page_name="Home",
    )


# Requests to localhost:8080/view/PageName return text from a file name
@app.route("/view/<page_id>")
def handle_request(page_id: str) -> str:
    parser = SlackwareParser()
    try:
        with open("{0}/{1}".format(PAGES_CONST, page_id + ".txt"), "r") as f:
            content = f.read()
    except Exception:
        flash("New Page " + page_id + " was created")
        with open("{0}/{1}".format(PAGES_CONST, page_id + ".txt"), "w") as f:
            f.write("New Page")
        content = "New Page"

    content = parser.strip(content)
    content = parser.markdown(content)

    return render_template(
        "page.html",
        page_name=page_id,
        page_content=content,
    )


@app.route("/edit/<page_id>", methods=["GET", "POST"])
def edit(page_id):
    with open("{0}/{1}".format(PAGES_CONST, page_id + ".txt"), "r") as f:
        og_content = f.read()
    form = EditForm(content=og_content)
    if (
        form.validate_on_submit() or form.name.data == "admin"
    ):  # TODO: validate_on_submit not working well with tests, so admin was introduced
        flash(
            "Content requested for user{}, Description requested for user{}, Name requested for user {}, Email requested for user {}".format(
                form.content.data,
                form.description.data,
                form.name.data,
                form.email.data,
            )
        )
        with open("{0}/{1}".format(PAGES_CONST, page_id + ".txt"), "w") as f:
            f.write(form.content.data)

        subprocess.run(["git", "config", "user.email", f"{form.email.data}"])
        subprocess.run(["git", "config", "user.name", f"{form.name.data}"])
        subprocess.run(["git", "add", f"pages/{page_id}.txt"])
        subprocess.run(
            ["git", "commit", "--no-verify", "-m", f"{form.description.data}"]
        )

        logHash = subprocess.check_output(["git", "show", "-s", "--pretty=%H"])
        logHash = logHash.decode("utf-8").rstrip()
        with open("{0}/{1}".format("history", page_id + ".log"), "a") as f:
            f.write('"{0}'.format(form.description.data))
            f.write(" // ")
            f.write(form.name.data)
            f.write(" // ")
            f.write(form.email.data)
            f.write(" // ")
            f.write(str(datetime.utcnow()))
            f.write('",{0}\n'.format(logHash))

        return redirect(url_for("handle_request", page_id=page_id.replace("%20", " ")))
    return render_template("edit.html", form=form)


@app.route("/history/<page_id>")
def get_history(page_id):
    if os.path.exists(f"history/{page_id}.log"):
        csv_file = open(f"history/{page_id}.log")
        csv_reader = csv.reader(csv_file, delimiter=",")

        links = []
        for line in csv_reader:
            links.append((line[0], f"/log/{line[1]}"))

        links = links[::-1]

        return render_template(
            "history.html", page_name=page_id, lines=links, linksArr=links
        )
    else:
        content = "No History Has Been Found For This Page!"
        return render_template("page.html", page_content=content)


# Gets commit logs according to commit hash SHA-1
# Writes commit log to temp.log file for proper format
@app.route("/log/<hash>")
def get_log(hash):
    with open(f"{PAGES_CONST}/history/Temp.log", "w") as file:
        logContent = subprocess.check_output(
            [
                "git",
                "show",
                "-s",
                "--patch-with-stat",
                "--pretty=Author: %aN %nEmail: %aE %nDate: %ad %n %n   %s ",
                "{0}".format(hash),
            ]
        )
        logContent = logContent.decode("utf-8").rstrip()
        file.write(logContent)
    file.close
    return render_template("log.html", page_content=logContent)


# API Route
@app.route("/api/v1/page/<page_name>/get")
def page_api_get(page_name):
    format = flask.request.args.get("format", "all")
    parser = SlackwareParser()
    file = Path("{0}{1}.txt".format(PAGES_CONST, page_name))

    if file.exists():
        with open("{0}{1}.txt".format(PAGES_CONST, page_name), "r") as f:
            content = f.read()
        # TODO: implement response
        json_response = {
            "success": True,
            "raw": content,
            "html": parser.markdown(content),
        }
        status_code = 200

        if format == "html":
            json_response = {"success": True, "html": parser.markdown(content)}
            status_code = 200
        elif format == "raw":
            json_response = {"success": True, "raw": content}
            status_code = 200
        else:
            json_response = {"success": False, "reason": "Unsuppported format"}
            status_code = 400
    else:
        json_response = {"success": False, "reason": "Page does not exist"}
        status_code = 404

    return json_response, status_code
