from flask_wtf import FlaskForm  # type: ignore
from wtforms import StringField, SubmitField  # type: ignore
from wtforms.validators import DataRequired  # type: ignore
from wtforms.widgets import TextArea  # type: ignore


class EditForm(FlaskForm):
    content = StringField("Content", widget=TextArea(), validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired()])
    submit = SubmitField("Confirm Edit")
