Flask==1.1.2
pytest==6.2.3
mypy==0.812
pytest-cov==2.11.1
black==21.4b2
pre-commit==2.12.1
flask-wtf==0.15.1